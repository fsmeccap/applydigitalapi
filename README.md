# STEPS

## didn't have time to finish it because I'm leaving early today, but I owed you the advance :)

* docker pull mongo:latest
* docker run -d -p 27017:27017 --name mongo mongo:latest
# inside project folder
* docker build -t springboot-mongodb:1.0 .
* docker run -p 8080:8080 --name springboot-mongodb --link mongo:mongo -d springboot-mongodb:1.0
## to view logs from root project
* docker logs springboot-mongodb
## to view data
* docker exec -it mongo bash
