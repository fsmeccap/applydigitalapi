package com.fsmeccap.springbootmongoskeletonapi.scheduled;

import com.fsmeccap.springbootmongoskeletonapi.dao.AlgoliaResponseRepository;
import com.fsmeccap.springbootmongoskeletonapi.dao.HitRepository;
import com.fsmeccap.springbootmongoskeletonapi.model.AlgoliaResponse;
import com.fsmeccap.springbootmongoskeletonapi.model.Hit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class Algolia {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private HitRepository hitRepository;

    // 0 * * * * cada hora
    @Scheduled(cron = "*/10 * * * * *")
    public void search_by_date() {
        AlgoliaResponse algoliaResponse = restTemplate.getForObject("https://hn.algolia.com/api/v1/search_by_date?query=java", AlgoliaResponse.class);
        hitRepository.deleteAll();
        hitRepository.saveAll(algoliaResponse.getHits());
    }

}