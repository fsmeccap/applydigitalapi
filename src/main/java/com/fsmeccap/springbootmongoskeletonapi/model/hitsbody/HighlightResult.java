package com.fsmeccap.springbootmongoskeletonapi.model.hitsbody;

public class HighlightResult {
    Author AuthorObject;
    CommentText comment_textObject;
    StoryTitle storyTitleObject;
    StoryUrl story_urlObject;


    // Getter Methods

    public Author getAuthor() {
        return AuthorObject;
    }

    public CommentText getComment_text() {
        return comment_textObject;
    }

    public StoryTitle getStory_title() {
        return storyTitleObject;
    }

    public StoryUrl getStory_url() {
        return story_urlObject;
    }

    // Setter Methods

    public void setAuthor(Author authorObject) {
        this.AuthorObject = authorObject;
    }

    public void setComment_text(CommentText comment_textObject) {
        this.comment_textObject = comment_textObject;
    }

    public void setStory_title(StoryTitle storyTitleObject) {
        this.storyTitleObject = storyTitleObject;
    }

    public void setStory_url(StoryUrl story_urlObject) {
        this.story_urlObject = story_urlObject;
    }
}
