package com.fsmeccap.springbootmongoskeletonapi.model;

import com.fsmeccap.springbootmongoskeletonapi.model.hitsbody.HighlightResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "hits")
public class Hit {

    private String created_at;
    private String title = null;
    private String url = null;
    private String author;
    private String points = null;
    private String story_text = null;
    private String comment_text;
    private String num_comments = null;
    private float story_id;
    private String story_title;
    private String story_url;
    private float parent_id;
    private float created_at_i;
    ArrayList<Object> _tags = new ArrayList<Object>();
    private String objectID;
    HighlightResult _highlightResultObject;


    // Getter Methods

    public String getCreated_at() {
        return created_at;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getAuthor() {
        return author;
    }

    public String getPoints() {
        return points;
    }

    public String getStory_text() {
        return story_text;
    }

    public String getComment_text() {
        return comment_text;
    }

    public String getNum_comments() {
        return num_comments;
    }

    public float getStory_id() {
        return story_id;
    }

    public String getStory_title() {
        return story_title;
    }

    public String getStory_url() {
        return story_url;
    }

    public float getParent_id() {
        return parent_id;
    }

    public float getCreated_at_i() {
        return created_at_i;
    }

    public String getObjectID() {
        return objectID;
    }

    public HighlightResult get_highlightResult() {
        return _highlightResultObject;
    }

    // Setter Methods

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public void setStory_text(String story_text) {
        this.story_text = story_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }

    public void setNum_comments(String num_comments) {
        this.num_comments = num_comments;
    }

    public void setStory_id(float story_id) {
        this.story_id = story_id;
    }

    public void setStory_title(String story_title) {
        this.story_title = story_title;
    }

    public void setStory_url(String story_url) {
        this.story_url = story_url;
    }

    public void setParent_id(float parent_id) {
        this.parent_id = parent_id;
    }

    public void setCreated_at_i(float created_at_i) {
        this.created_at_i = created_at_i;
    }

    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }

    public void set_highlightResult(HighlightResult _highlightResultObject) {
        this._highlightResultObject = _highlightResultObject;
    }
}