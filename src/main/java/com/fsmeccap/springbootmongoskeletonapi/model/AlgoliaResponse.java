package com.fsmeccap.springbootmongoskeletonapi.model;

import com.fsmeccap.springbootmongoskeletonapi.model.algoliaresponsesbody.Exhaustive;
import com.fsmeccap.springbootmongoskeletonapi.model.algoliaresponsesbody.ProcessingTimingsMS;

import java.util.ArrayList;
import java.util.List;

//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Document(collection = "algoliaResponses")
public class AlgoliaResponse {

    ArrayList<Hit> hits = new ArrayList<Hit>();
    private float nbHits;
    private float page;
    private float nbPages;
    private float hitsPerPage;
    private boolean exhaustiveNbHits;
    private boolean exhaustiveTypo;
    Exhaustive ExhaustiveObject;
    private String query;
    private String params;
    private float processingTimeMS;
    ProcessingTimingsMS ProcessingTimingsMSObject;


    // Getter Methods

    public List<Hit> getHits() {
        return hits;
    }

    public float getNbHits() {
        return nbHits;
    }

    public float getPage() {
        return page;
    }

    public float getNbPages() {
        return nbPages;
    }

    public float getHitsPerPage() {
        return hitsPerPage;
    }

    public boolean getExhaustiveNbHits() {
        return exhaustiveNbHits;
    }

    public boolean getExhaustiveTypo() {
        return exhaustiveTypo;
    }

    public Exhaustive getExhaustive() {
        return ExhaustiveObject;
    }

    public String getQuery() {
        return query;
    }

    public String getParams() {
        return params;
    }

    public float getProcessingTimeMS() {
        return processingTimeMS;
    }

    public ProcessingTimingsMS getProcessingTimingsMS() {
        return ProcessingTimingsMSObject;
    }

    // Setter Methods

    public void setNbHits(float nbHits) {
        this.nbHits = nbHits;
    }

    public void setPage(float page) {
        this.page = page;
    }

    public void setNbPages(float nbPages) {
        this.nbPages = nbPages;
    }

    public void setHitsPerPage(float hitsPerPage) {
        this.hitsPerPage = hitsPerPage;
    }

    public void setExhaustiveNbHits(boolean exhaustiveNbHits) {
        this.exhaustiveNbHits = exhaustiveNbHits;
    }

    public void setExhaustiveTypo(boolean exhaustiveTypo) {
        this.exhaustiveTypo = exhaustiveTypo;
    }

    public void setExhaustive(Exhaustive exhaustiveObject) {
        this.ExhaustiveObject = exhaustiveObject;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public void setProcessingTimeMS(float processingTimeMS) {
        this.processingTimeMS = processingTimeMS;
    }

    public void setProcessingTimingsMS(ProcessingTimingsMS processingTimingsMSObject) {
        this.ProcessingTimingsMSObject = processingTimingsMSObject;
    }
}

