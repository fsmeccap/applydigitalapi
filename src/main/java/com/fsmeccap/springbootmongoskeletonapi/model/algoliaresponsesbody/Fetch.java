package com.fsmeccap.springbootmongoskeletonapi.model.algoliaresponsesbody;

public class Fetch {
    private float total;


    // Getter Methods

    public float getTotal() {
        return total;
    }

    // Setter Methods

    public void setTotal(float total) {
        this.total = total;
    }
}
