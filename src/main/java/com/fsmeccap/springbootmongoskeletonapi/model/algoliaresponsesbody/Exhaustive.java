package com.fsmeccap.springbootmongoskeletonapi.model.algoliaresponsesbody;

public class Exhaustive {
    private boolean nbHits;
    private boolean typo;


    // Getter Methods

    public boolean getNbHits() {
        return nbHits;
    }

    public boolean getTypo() {
        return typo;
    }

    // Setter Methods

    public void setNbHits(boolean nbHits) {
        this.nbHits = nbHits;
    }

    public void setTypo(boolean typo) {
        this.typo = typo;
    }


}
