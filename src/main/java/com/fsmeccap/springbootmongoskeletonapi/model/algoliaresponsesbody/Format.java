package com.fsmeccap.springbootmongoskeletonapi.model.algoliaresponsesbody;

public class Format {
    private float highlighting;
    private float total;


    // Getter Methods

    public float getHighlighting() {
        return highlighting;
    }

    public float getTotal() {
        return total;
    }

    // Setter Methods

    public void setHighlighting(float highlighting) {
        this.highlighting = highlighting;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
