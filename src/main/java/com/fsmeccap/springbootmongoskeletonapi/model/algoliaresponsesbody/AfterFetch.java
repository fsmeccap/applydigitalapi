package com.fsmeccap.springbootmongoskeletonapi.model.algoliaresponsesbody;

public class AfterFetch {
    Format FormatObject;
    private float total;


    // Getter Methods

    public Format getFormat() {
        return FormatObject;
    }

    public float getTotal() {
        return total;
    }

    // Setter Methods

    public void setFormat(Format formatObject) {
        this.FormatObject = formatObject;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
