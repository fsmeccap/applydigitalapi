package com.fsmeccap.springbootmongoskeletonapi.model.algoliaresponsesbody;

public class ProcessingTimingsMS {
    AfterFetch AfterFetchObject;
    Fetch FetchObject;
    private float total;


    // Getter Methods

    public AfterFetch getAfterFetch() {
        return AfterFetchObject;
    }

    public Fetch getFetch() {
        return FetchObject;
    }

    public float getTotal() {
        return total;
    }

    // Setter Methods

    public void setAfterFetch(AfterFetch afterFetchObject) {
        this.AfterFetchObject = afterFetchObject;
    }

    public void setFetch(Fetch fetchObject) {
        this.FetchObject = fetchObject;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
