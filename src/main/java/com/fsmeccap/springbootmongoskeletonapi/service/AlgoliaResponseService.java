package com.fsmeccap.springbootmongoskeletonapi.service;

import com.fsmeccap.springbootmongoskeletonapi.dao.HitRepository;
import com.fsmeccap.springbootmongoskeletonapi.model.Hit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("/algolia")
public class AlgoliaResponseService {

    @Autowired
    private HitRepository hitRepository;

    @PostMapping
    public Hit saveHit(@RequestBody Hit hit) {
        return hitRepository.save(hit);
    }

    @GetMapping
    public List<Hit> getHits() {
        return hitRepository.findAll();
    }

    @GetMapping("/created_at")
    public List<Hit> created_at(@RequestParam String month) throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat("MMMM");
        Calendar cal = Calendar.getInstance();
        cal.setTime(inputFormat.parse(month));
        SimpleDateFormat outputFormat = new SimpleDateFormat("MM");
        String monthNumber = outputFormat.format(cal.getTime());
        return hitRepository.findByCreated_atContains(monthNumber);
    }

    @GetMapping("/author")
    public List<Hit> author(@RequestParam String author) throws ParseException {
        return hitRepository.findByAuthor(author);
    }

    @GetMapping("/tags")
    public List<Hit> tags(@RequestParam ArrayList<Object> tags) throws ParseException {
        return hitRepository.findBy_tagsContains(tags);
    }

    @GetMapping("/title")
    public List<Hit> title(@RequestParam String title) throws ParseException {
        return hitRepository.findByStory_titleContains(title);
    }

}
