package com.fsmeccap.springbootmongoskeletonapi.dao;

import com.fsmeccap.springbootmongoskeletonapi.model.AlgoliaResponse;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AlgoliaResponseRepository extends MongoRepository<AlgoliaResponse,Integer> {
}
