package com.fsmeccap.springbootmongoskeletonapi.dao;

import com.fsmeccap.springbootmongoskeletonapi.model.Hit;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.ArrayList;
import java.util.List;

public interface HitRepository extends MongoRepository<Hit,Integer> {

    List<Hit> findByAuthor(String author);
    List<Hit> findBy_tagsContains(ArrayList<Object> _tags);
    List<Hit> findByStory_titleContains(String author);
    List<Hit> findByCreated_atContains(String month);

}
